SOURCES	     += ../sound_source_id/__main__.py
SOURCES	     += ../sound_source_id/audio_manager.py
SOURCES	     += ../sound_source_id/dialog_edit_transducers.py
SOURCES	     += ../sound_source_id/dialog_edit_preferences.py
SOURCES	     += ../sound_source_id/global_parameters.py
SOURCES	     += ../sound_source_id/sndlib.py
TRANSLATIONS += sound_source_id_it.ts sound_source_id_fr.ts sound_source_id_es.ts sound_source_id_el.ts sound_source_id_en_US.ts sound_source_id_en_GB.ts
