<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message>
        <location filename="../sound_source_id/global_parameters.py" line="167"/>
        <source>custom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences Window</name>
</context>
<context>
    <name>applicationWindow</name>
    <message>
        <location filename="../sound_source_id/__main__.py" line="149"/>
        <source>sound_source_id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="169"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="277"/>
        <source>Load Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="173"/>
        <source>Load Parameters File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="178"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="180"/>
        <source>Exit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="186"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="187"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="196"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="198"/>
        <source>Manual (html)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="202"/>
        <source>Manual (pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="964"/>
        <source>About sound_source_id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="423"/>
        <source>Loaded </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="423"/>
        <source> parameter file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="436"/>
        <source>Choose parameters file to load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="436"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="548"/>
        <source>Choose file to write results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="603"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="581"/>
        <source>You need to select a file where to save the results to proceed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="585"/>
        <source>Please, enter the listener&apos;s name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="586"/>
        <source>Input Dialog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="741"/>
        <source>Running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="853"/>
        <source>Run Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="856"/>
        <source>Finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="964"/>
        <source>&lt;b&gt;sound_source_id - Python app for sound localization experiments&lt;/b&gt; &lt;br&gt;
                              - version: {0}; &lt;br&gt;
                              - build date: {1} &lt;br&gt;
                              &lt;p&gt; Copyright &amp;copy; 2022-2023 Samuele Carcagno. &lt;a href=&quot;mailto:sam.carcagno@gmail.com&quot;&gt;sam.carcagno@gmail.com&lt;/a&gt; 
                              All rights reserved. &lt;p&gt;
                              This program is free software: you can redistribute it and/or modify
                              it under the terms of the GNU General Public License as published by
                              the Free Software Foundation, either version 3 of the License, or
                              (at your option) any later version.
                              &lt;p&gt;
                              This program is distributed in the hope that it will be useful,
                              but WITHOUT ANY WARRANTY; without even the implied warranty of
                              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                              GNU General Public License for more details.
                              &lt;p&gt;
                              You should have received a copy of the GNU General Public License
                              along with this program.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;
                              &lt;p&gt;Python {2} - {3} {4} compiled against Qt {5}, and running with Qt {6} on {7}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="615"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="293"/>
        <source>Run Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="253"/>
        <source>There was an issue loading your default parameter file. It is likely to contain an error. Please select a new default parameters file and restart the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="268"/>
        <source>The default parameters file could not be found. Please select a new default parameters file and restart the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="210"/>
        <source>Show Play Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="191"/>
        <source>Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="230"/>
        <source>Listener:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="214"/>
        <source>Show Response Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="219"/>
        <source>Show Response Lights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="282"/>
        <source>Choose Results File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="288"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="603"/>
        <source>This will stop the current test. Are you sure you want to proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="572"/>
        <source>The results file cannot be changed while a test is running. Reset the test and then choose a new results file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="548"/>
        <source>csv (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="553"/>
        <source>File </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="553"/>
        <source>already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="224"/>
        <source>Show Control Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>preferencesDialog</name>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="65"/>
        <source>Language (requires restart):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="73"/>
        <source>Country (requires restart):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="82"/>
        <source>Default PRM File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="91"/>
        <source>CSV separator:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="122"/>
        <source>Play Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="127"/>
        <source>Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="329"/>
        <source>custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="156"/>
        <source>Device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="173"/>
        <source>Buffer Size (samples):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="190"/>
        <source>WAV Manager (requires restart):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="207"/>
        <source>Append silence to each sound (ms):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="213"/>
        <source>Prepend silence to each sound (ms):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="224"/>
        <source>Genera&amp;l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="225"/>
        <source>Soun&amp;d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="243"/>
        <source>Find file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="349"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="349"/>
        <source>There are unsaved changes. Apply Changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_preferences.py" line="113"/>
        <source>Transducers:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>responseLight</name>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1146"/>
        <source>Light &amp; Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1122"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1158"/>
        <source>Light &amp; Smiley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1180"/>
        <source>Light &amp; Text &amp; Smiley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1128"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1165"/>
        <source>Text &amp; Smiley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/__main__.py" line="1139"/>
        <source>Smiley</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>transducersDialog</name>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="64"/>
        <source>Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="64"/>
        <source>Max Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="69"/>
        <source>Rename Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="72"/>
        <source>Change Max Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="76"/>
        <source>Add Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="79"/>
        <source>Remove Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="167"/>
        <source>Edit Transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="249"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="179"/>
        <source>Only one label can be renamed at a time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="184"/>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="198"/>
        <source>Input Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="192"/>
        <source>Only one item can be edited at a time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="197"/>
        <source>Level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sound_source_id/dialog_edit_transducers.py" line="249"/>
        <source>Only one transducer left. Cannot remove!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
