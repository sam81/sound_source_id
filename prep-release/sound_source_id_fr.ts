<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
  <context>
    <name />
    <message>
      <location filename="../sound_source_id/global_parameters.py" line="168" />
      <location filename="../sound_source_id/global_parameters.py" line="165" />
      <location filename="../sound_source_id/global_parameters.py" line="88" />
      <source>custom</source>
      <translation type="unfinished">personnalisé</translation>
    </message>
  </context>
  <context>
    <name>Preferences Window</name>
    <message>
      <source>System Settings</source>
      <translation type="vanished">Paramètre du système</translation>
    </message>
  </context>
  <context>
    <name>applicationWindow</name>
    <message>
      <location filename="../sound_source_id/__main__.py" line="150" />
      <source>sound_source_id</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="170" />
      <source>&amp;File</source>
      <translation>&amp;Fichier</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="278" />
      <location filename="../sound_source_id/__main__.py" line="172" />
      <source>Load Parameters</source>
      <translation>Charger les paramètres</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="174" />
      <source>Load Parameters File</source>
      <translation>Charger le fichier des paramètres</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="179" />
      <source>Exit</source>
      <translation>Quitter</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="181" />
      <source>Exit application</source>
      <translation>Quitter l'application</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="187" />
      <source>&amp;Edit</source>
      <translation>&amp;Édition</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="188" />
      <source>Preferences</source>
      <translation>Préférences</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="192" />
      <source>Transducers</source>
      <translation>Transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="197" />
      <source>&amp;Help</source>
      <translation>&amp;Aide</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="199" />
      <source>Manual (html)</source>
      <translation>Manuel (html)</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="203" />
      <source>Manual (pdf)</source>
      <translation>Manuel (pdf)</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="967" />
      <location filename="../sound_source_id/__main__.py" line="207" />
      <source>About sound_source_id</source>
      <translation>À propos de sound_source_id</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="211" />
      <source>Show Play Buttons</source>
      <translation>Afficher les boutons de lecture</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="215" />
      <source>Show Response Buttons</source>
      <translation>Afficher les boutons de réponse</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="220" />
      <source>Show Response Lights</source>
      <translation>Affichier les lumières de réponse</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="225" />
      <source>Show Control Panel</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="231" />
      <source>Listener:</source>
      <translation>Participant :</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="606" />
      <location filename="../sound_source_id/__main__.py" line="584" />
      <location filename="../sound_source_id/__main__.py" line="575" />
      <location filename="../sound_source_id/__main__.py" line="556" />
      <location filename="../sound_source_id/__main__.py" line="430" />
      <location filename="../sound_source_id/__main__.py" line="269" />
      <location filename="../sound_source_id/__main__.py" line="254" />
      <source>Warning</source>
      <translation>Avertissement</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="254" />
      <source>There was an issue loading your default parameter file. It is likely to contain an error. Please select a new default parameters file and restart the program.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="269" />
      <source>The default parameters file could not be found. Please select a new default parameters file and restart the program.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="618" />
      <location filename="../sound_source_id/__main__.py" line="605" />
      <location filename="../sound_source_id/__main__.py" line="429" />
      <location filename="../sound_source_id/__main__.py" line="273" />
      <source>Start</source>
      <translation>Commencer</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="283" />
      <source>Choose Results File</source>
      <translation>Choisir le fichier des résultats</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="289" />
      <source>Reset</source>
      <translation>Réinitialiser</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="294" />
      <source>Run Demo</source>
      <translation>Démo</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="426" />
      <source>Loaded </source>
      <translation>Chargé</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="426" />
      <source> parameter file</source>
      <translation>fichier des paramètres</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="859" />
      <location filename="../sound_source_id/__main__.py" line="605" />
      <location filename="../sound_source_id/__main__.py" line="429" />
      <source>Finished</source>
      <translation>Fini</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="606" />
      <location filename="../sound_source_id/__main__.py" line="430" />
      <source>This will stop the current test. Are you sure you want to proceed?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="439" />
      <location filename="../sound_source_id/__main__.py" line="434" />
      <source>Choose parameters file to load</source>
      <translation>Choisir le fichier des paramètres à charger</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="439" />
      <location filename="../sound_source_id/__main__.py" line="434" />
      <source>All Files (*)</source>
      <translation>Tous les fichier (*)</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="551" />
      <source>Choose file to write results</source>
      <translation>Choisir le fichier des résultats</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="551" />
      <source>csv (*.csv)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="556" />
      <source>File </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="556" />
      <source>already exists. Do you want to overwrite it?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="575" />
      <source>The results file cannot be changed while a test is running. Reset the test and then choose a new results file.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="584" />
      <source>You need to select a file where to save the results to proceed</source>
      <translation>Vous devez choisir un fichier où sauvegarder les résultats</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="588" />
      <source>Please, enter the listener's name:</source>
      <translation>S'il vous plaît de saisir l'identifiant du sujet</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="589" />
      <source>Input Dialog:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="744" />
      <location filename="../sound_source_id/__main__.py" line="600" />
      <location filename="../sound_source_id/__main__.py" line="596" />
      <source>Running</source>
      <translation>En exécution</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="856" />
      <source>Run Block</source>
      <translation>Lancer Bloc</translation>
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="968" />
      <source>&lt;b&gt;sound_source_id - Python app for sound localization experiments&lt;/b&gt; &lt;br&gt;
                              - version: {0}; &lt;br&gt;
                              - build date: {1} &lt;br&gt;
                              &lt;p&gt; Copyright &amp;copy; 2022-2024 Samuele Carcagno. &lt;a href="mailto:sam.carcagno@gmail.com"&gt;sam.carcagno@gmail.com&lt;/a&gt; 
                              All rights reserved. &lt;p&gt;
                              This program is free software: you can redistribute it and/or modify
                              it under the terms of the GNU General Public License as published by
                              the Free Software Foundation, either version 3 of the License, or
                              (at your option) any later version.
                              &lt;p&gt;
                              This program is distributed in the hope that it will be useful,
                              but WITHOUT ANY WARRANTY; without even the implied warranty of
                              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                              GNU General Public License for more details.
                              &lt;p&gt;
                              You should have received a copy of the GNU General Public License
                              along with this program.  If not, see &lt;a href="http://www.gnu.org/licenses/"&gt;http://www.gnu.org/licenses/&lt;/a&gt;
                              &lt;p&gt;Python {2} - {3} {4} compiled against Qt {5}, and running with Qt {6} on {7}</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>phonesDialog</name>
    <message>
      <source>Warning</source>
      <translation type="vanished">Avertissement</translation>
    </message>
  </context>
  <context>
    <name>preferencesDialog</name>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="65" />
      <source>Language (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="73" />
      <source>Country (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="82" />
      <source>Default PRM File:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="91" />
      <source>CSV separator:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="113" />
      <source>Transducers:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="122" />
      <source>Play Command:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="127" />
      <source>Command:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="329" />
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="249" />
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="135" />
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="130" />
      <source>custom</source>
      <translation type="unfinished">personnalisé</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="156" />
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="142" />
      <source>Device:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="173" />
      <source>Buffer Size (samples):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="190" />
      <source>WAV Manager (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="207" />
      <source>Append silence to each sound (ms):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="213" />
      <source>Prepend silence to each sound (ms):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="224" />
      <source>Genera&amp;l</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="225" />
      <source>Soun&amp;d</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="243" />
      <source>Find file</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="349" />
      <source>Warning</source>
      <translation type="unfinished">Avertissement</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_preferences.py" line="349" />
      <source>There are unsaved changes. Apply Changes?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Phones:</source>
      <translation type="vanished">Phones:</translation>
    </message>
  </context>
  <context>
    <name>responseLight</name>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1149" />
      <location filename="../sound_source_id/__main__.py" line="1088" />
      <location filename="../sound_source_id/__main__.py" line="1079" />
      <location filename="../sound_source_id/__main__.py" line="1065" />
      <location filename="../sound_source_id/__main__.py" line="1052" />
      <source>Light &amp; Text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1125" />
      <location filename="../sound_source_id/__main__.py" line="1079" />
      <source>Light</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1161" />
      <location filename="../sound_source_id/__main__.py" line="1113" />
      <location filename="../sound_source_id/__main__.py" line="1079" />
      <source>Light &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1183" />
      <location filename="../sound_source_id/__main__.py" line="1113" />
      <location filename="../sound_source_id/__main__.py" line="1088" />
      <location filename="../sound_source_id/__main__.py" line="1079" />
      <source>Light &amp; Text &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1131" />
      <location filename="../sound_source_id/__main__.py" line="1088" />
      <source>Text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1168" />
      <location filename="../sound_source_id/__main__.py" line="1113" />
      <location filename="../sound_source_id/__main__.py" line="1088" />
      <source>Text &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/__main__.py" line="1142" />
      <location filename="../sound_source_id/__main__.py" line="1113" />
      <source>Smiley</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>transducersDialog</name>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="64" />
      <source>Transducers</source>
      <translation>Transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="64" />
      <source>Max Level</source>
      <translation>Niveau maximal</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="69" />
      <source>Rename Transducers</source>
      <translation>Renommez les transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="72" />
      <source>Change Max Level</source>
      <translation>Cahngez le niveau maximal</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="76" />
      <source>Add Transducers</source>
      <translation>Ajouter des transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="79" />
      <source>Remove Transducers</source>
      <translation>Éliminer les transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="167" />
      <source>Edit Transducers</source>
      <translation>Modifier les transducteurs</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="249" />
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="192" />
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="179" />
      <source>Warning</source>
      <translation>Avertissement</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="179" />
      <source>Only one label can be renamed at a time</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="184" />
      <source>New name:</source>
      <translation>Nouveau nom :</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="198" />
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="185" />
      <source>Input Dialog</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="192" />
      <source>Only one item can be edited at a time</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="197" />
      <source>Level:</source>
      <translation>Niveau :</translation>
    </message>
    <message>
      <location filename="../sound_source_id/dialog_edit_transducers.py" line="250" />
      <source>Only one transducer left. Cannot remove!</source>
      <translation type="unfinished" />
    </message>
  </context>
</TS>
