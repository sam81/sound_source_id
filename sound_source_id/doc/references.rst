.. raw:: latex

   \cleardoublepage
   \begingroup
   \renewcommand\chapter[1]{\endgroup}
   \phantomsection

.. _refs:

**********
References
**********

.. [HartmannEtAl1998] Hartmann, W. M., Rakerd, B., & Gaalaas, J. B. (1998). On the source-identification method. The Journal of the Acoustical Society of America, 104(6), 3546–3557. https://doi.org/10.1121/1.423936
 
