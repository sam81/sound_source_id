.. sound_source_id documentation master file, created by
   sphinx-quickstart on Thu Feb 23 16:00:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sound_source_id's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   experiment_setup
   results_files
   calibration
   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
